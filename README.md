# Tulipe By KImtai
Tulipe is a clean and modern Free responsive app landing-page template for Mobile App. Built with Bootstrap 4.x frontend Framework. The codebase is well organized and SEO optimized.



👉🏻[View Live Preview](https://tulipe-kimtai-developer-c3a08928e48327d05f36725c9ffbaed4a43e082.gitlab.io)

<!-- resources -->
## Pages
* **Homepage 1**


<!-- resources -->
## Resources
Some third-party plugins that we used to build this template. Please check their licence.
* **Bootstrap v4.5**: https://getbootstrap.com/docs/4.5/getting-started/introduction/
* **Jquery v3.5.1**: https://jquery.com/download/
* **Themify Icons**: https://themify.me/themify-icons
* **Google Fonts**: http://fonts.google.com/
* **AOS**: https://michalsnik.github.io/aos/
* **Fancybox**: http://fancyapps.com/fancybox/
* **Slick**: https://kenwheeler.github.io/slick/
* **SyoTimer**: http://syomochkin.xyz/folio/syotimer/demo.html